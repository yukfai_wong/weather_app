import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { green } from '@material-ui/core/colors';
import MuiAlert from '@material-ui/lab/Alert';

import Container from '@material-ui/core/Container';
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import Fab from '@material-ui/core/Fab';
import Fade from '@material-ui/core/Fade';
import Box from '@material-ui/core/Box';
import Typography from "@material-ui/core/Typography";
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';


import { useCookies, CookiesProvider  } from 'react-cookie';

import CircularProgress from '@material-ui/core/CircularProgress';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import MenuIcon from '@material-ui/icons/Menu';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}



function ControlButton() {
  const [cookies, setCookie] = useCookies();

  const [playing, setplaying] = React.useState(false);//control of showing play(false) of pause button(true)
  const [loading, setLoading] = React.useState(false);//control of showing loading animation

  const [cookie, setcookie] = React.useState(cookies.autoload === "true");//true if user want to auto play audio report
  const [firsttime, setfirsttime] = React.useState(true);//control whether the button is clicked without audio file cached in browser

  const [audioPermissionblock, setaudioPermissionblock] = React.useState(false);//control of showing error message for browser denied autoplay
  const [audioPermissionSuccess, setaudioPermissionSuccess] = React.useState(false);//control of showing sucess message for browser allow autoplay

  const report_player = document.querySelector('audio#weather_report');

  function handlePlayClick(e) {
    if(playing) {
      report_player.pause();//pause the audio
      setplaying(false);//change the button to play
    } else {
      if(firsttime){
        //when the play button is clicked for the first time
        //show the loading animation
        setLoading(true);
        setfirsttime(false);
      }
      report_player.play();//play the audio
      setplaying(true);//change the button to pause
    }
  };

  const handleClosePermissionblock = (event, reason) => {
    setaudioPermissionblock(false);//turn off the message
  };
  const handleClosePermissionSuccess = (event, reason) => {
    setaudioPermissionSuccess(false);//turn off the message
  };

  useEffect(() => {
    report_player.oncanplay = function() {
      setLoading(false);//loading finish disable the loading animation
    };
    report_player.onplaying = function() {
      setplaying(true);//audio is playing change into play button
    };
    report_player.onpause = function() {
      setplaying(false);//audio is not playing change into pause button
    };
    report_player.onended = function() {
      setplaying(false);//audio is not playing change into pause button
    };
    if(cookie){
      setcookie(false);//avoid mutiple play when the element is re-rendering
      setLoading(true);
      //show the animation before play(),
      //becasue promise.then will called ""after"" the audio is playered
      var promise = report_player.play();
      if (promise !== undefined) {
        promise.then(_ => {
          //message when browser allow auto play
          setfirsttime(false);
          setaudioPermissionSuccess(true);//sucess message
        }).catch(error => {
          //browser denied auto play
          setLoading(false);
          setaudioPermissionblock(true);//error message
        });
      }
    };
  });
  return (
    <div>
      <Snackbar open={audioPermissionblock} autoHideDuration={7000} onClose={handleClosePermissionblock}>
        <Alert onClose={handleClosePermissionblock} severity="error">
          Permission block of automatic play is detected. Please check your brower permission.
        </Alert>
      </Snackbar>
      <Snackbar open={audioPermissionSuccess} autoHideDuration={7000} onClose={handleClosePermissionSuccess}>
        <Alert onClose={handleClosePermissionSuccess} severity="success">
          Automatically playing the weather report for you ;)
        </Alert>
      </Snackbar>
      <Fab color="primary" aria-label="play" onClick={handlePlayClick}>
        {playing ? <PauseIcon style={{ fontSize: 35 }}/> : <PlayArrowIcon style={{ fontSize: 35 }} />}
      </Fab>
      {loading && <CircularProgress size={68} style={{color: green[500], position: 'absolute', top: -6, left: -6, zIndex: 1,}} />}
    </div>
  );
};

function MenuButton() {
  const [cookies, setCookie] = useCookies();//load cookies

  const [anchorEl, setAnchorEl] = React.useState(document.querySelector("#menu_button"));//for the menu to know where it should be appear
  const [open, setOpen] = React.useState(cookies.autoload === undefined);// control of whether the menu should open.
  //it autoload cookies is not set, it will always show, when user load the websit

  const handleNoClick = () => (event) => {
    //user choose yes, store the "false" cookie
    setCookie("autoload", false, {path: '/', maxAge: 2147483647});
    setOpen((prev) => !prev);
  };

  const handleYesClick = () => (event) => {
    //user choose yes, store the "true" cookie
    setCookie("autoload", true, {path: '/', maxAge: 2147483647});
    setOpen((prev) => !prev);
  };

  const handleClick = () => (event) => {
    //user click the menu button, open / close the menu
    setOpen((prev) => !prev);
  };
  return (
    <div>
    <CookiesProvider>
      <Popper open={open} anchorEl={anchorEl} placement='top-End' transition>
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
              <Paper style={{ width: "400px" }}>
                <Grid
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="baseline"
                >
                  <Grid container >
                    <Typography style={{padding: "16px"}}>Do you want us to play the audio report when you open the website?</Typography>
                  </Grid>
                  <Grid container >
                    <Typography style={{padding: "16px"}}>The website will save a cookie to remember this option.</Typography>
                  </Grid>
                  <Grid container >
                    <Typography style={{padding: "16px"}}>If your browser did not Automatically play the report, please check you browser setting.</Typography>
                  </Grid>
                  <Grid container >
                    <Button color="primary" onClick={handleYesClick()} style={{margin: "auto", display: "block"}}>yes</Button>
                    <Button color="primary" onClick={handleNoClick()} style={{margin: "auto", display: "block"}}>no</Button>
                  </Grid>
                </Grid>
              </Paper>
          </Fade>
        )}
      </Popper>
      <Fab color="primary" aria-label="Menu" onClick={handleClick()}>
        <MenuIcon />
      </Fab>
    </CookiesProvider>
    </div>
  );
};

function Player() {
  const [playerFailed, setplayerFailed] = React.useState(false);//control of showing error message

  const handleClosePlayerFailed = (event, reason) => {
    setplayerFailed(false);
  };

  function playerError(){
    //triggered by source onError
    setplayerFailed(true);
  }
  return (
    <div>
    <audio id="weather_report" preload="none" >
      <source src="/voice.mp3" type="audio/mpeg" onError={playerError} />
      Your browser does not support the audio element.
    </audio>
    <Snackbar open={playerFailed} autoHideDuration={10000} onClose={handleClosePlayerFailed}>
      <Alert onClose={handleClosePlayerFailed} severity="error">
        Sorry, look like something go wrong while we are try to play the weather report for you. Please try again later.
      </Alert>
    </Snackbar>
    </div>
  );
};

ReactDOM.hydrate(<Player />, document.querySelector('#player'));
ReactDOM.hydrate(<ControlButton />, document.querySelector('#control_button'));
ReactDOM.hydrate(<MenuButton />, document.querySelector('#menu_button'));
