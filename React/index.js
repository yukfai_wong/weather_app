import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import MenuIcon from '@material-ui/icons/Menu';

import Fab from '@material-ui/core/Fab';
import Box from '@material-ui/core/Box';
import Popper from '@material-ui/core/Popper';

import Weather from './weather_report'

function Copyright() {
  return (
      <Box textAlign="center" color="textSecondary" style={{marginTop: "30px", width: "inherit"}}>
        {'Copyright © '}
        <Link color="inherit" href="/">
          Weather APP
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Box>
  );
}

function FloatingMenuButton() {
  //menu button
  return (
    <Box id="menu_button" position="fixed" right="30px" top="85%">
      <div>
        <Fab color="primary" aria-label="Menu">
          <MenuIcon style={{ fontSize: 35 }}/>
        </Fab>
      </div>
    </Box>
  );
};

function FloatingActionButtonZoom() {
  //menu play / pause button
  return (
    <Box id="control_button"  position="fixed" right="100px" top="85%">
      <div>
        <Fab color="primary"  aria-label="play">
          <PlayArrowIcon style={{ fontSize: 35 }}/>
        </Fab>
      </div>
    </Box>
  );
};

function Audioplayer() {
  //audio player
  return (
    <div id="player">
    <div>
    <audio id="weather_report" preload="none">
      <source src="/voice.mp3" type="audio/mpeg" />
      Your browser does not support the audio element.
    </audio>
    </div>
    </div>
  );
}

export default function Index(props) {
  //layout of the pages
  return (
    <Container style={{ minWidth: 800, maxWidth: 900}}>
      <Weather weather_data={props.weather_data}/>
      <FloatingMenuButton />
      <FloatingActionButtonZoom />
      <Audioplayer />
      <Copyright />
    </Container>
  );
}
