import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import LocationIcon from '@material-ui/icons/MyLocation';
import AccessTimeIcon from '@material-ui/icons/AccessTime';

import dateformat from 'dateformat';

const Sunny ="/weather_icon/Sunny.svg";
const Moon ="/weather_icon/Moon.svg";

const Cloudy ="/weather_icon/Cloudy.svg";
const Cloudy_Sunny ="/weather_icon/Cloudy_Sunny.svg";
const Cloudy_Moon ="/weather_icon/Cloudy_Moon.svg";
const Cloudy_broken ="/weather_icon/Cloudy_broken.svg";
const Cloudy_Lightning ="/weather_icon/Cloudy_Lightning.svg";

const Raining ="/weather_icon/Raining.svg";
const Raining_Sun ="/weather_icon/Raining_Sun.svg";
const Raining_Moon ="/weather_icon/Raining_Moon.svg";
const Raining_Light ="/weather_icon/Raining_Light.svg";

const Foggy ="/weather_icon/Foggy.svg";
const Snowing ="/weather_icon/Snow_Clody.svg";

const Temperature ="/weather_icon/Temperature.svg";
const Humidity ="/weather_icon/humidity.svg";
const Arrow ="/weather_icon/arrow_top.svg";
const Pressure ="/weather_icon/pressure.svg";
const Uvi ="/weather_icon/uvi.svg";
const Visibility = "/weather_icon/visibility.svg";

function inRange(sm, num, lg){
  return ((sm<=num)&&(num<=lg));
};


function Weather_icon(props) {
//paser for the weather code to weathe image
//https://openweathermap.org/weather-conditions
  const id = props.id;
  const d = props.d;
  let path, alt;
  switch(true){
    case (inRange(200, id, 232)):
      path = Cloudy_Lightning; alt = "Cloudy Lightning";
      break;
    case (inRange(300, id, 321)):
      if(d=="d"){
        path = Raining_Sun; alt = "Raining Sun";
      }else{
        path = Raining_Moon; alt = "Raining Moon";
      };
      break;
    case (inRange(500, id, 504)):
      path = Raining_Light; alt = "Rain Light";
      break;
    case (inRange(520, id, 531))://scattered clouds
      path = Raining; alt = "Raining";
      break;
    case (id == 511)://scattered clouds
      path = Snowing; alt = "Snowing";
      break;
    case (inRange(600, id, 622)):
      path = Snowing; alt = "Snowing";
      break;
    case (inRange(701, id, 781)):
      path = Foggy; alt = "Foggy";
      break;
    case (id == 800):
      if(d=="d"){
        path = Sunny; alt = "Sunny";
      }else{
        path = Moon; alt = "Moon";
      };
      break;
    case (id == 801):
      path = Cloudy_broken; alt = "Cloudy_broken";
      break;
    case (id == 802):
      if(d=="d"){
        path = Cloudy_Sunny; alt = "Cloudy Sunny";
      }else{
        path = Cloudy_Moon; alt = "Cloudy Moon";
      };
      break;
    case (inRange(803, id, 804)):
      path = Cloudy; alt = "Cloudy";
      break;
  };
  return <img src={path} alt={alt} style={props.style} />;
};

const arr=["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
function degToCard(degrees){//pase degree to cardinal direction
  degrees *= 10;
  return arr[Math.round(Math.round((degrees % 3600)/ 225))];
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const options = { hour: 'numeric', minute: 'numeric', timeZoneName: 'short',
                weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }// format of displaying data string in the webpage

export default function Weather(props) {
  const weather_data = props.weather_data;

  //pase the JSON weather data
  const weather_timezone = weather_data.timezone;
  const location = weather_timezone.replace("/", ", ").replace("_", " ");
  const weather_current = weather_data.current;

  options.timeZone = weather_timezone
  const date_current = new Date(weather_current.dt*1000);
  const date_current_str = date_current.toLocaleString('en', options)

  const weather = weather_current.weather[0].main;
  const weather_description  = weather_current.weather[0].description ;
  const weather_id = weather_current.weather[0].id;
  const weather_d = weather_current.weather[0].icon.slice(-1);;

  const temp_current = weather_current.temp;
  const temp_feel_current = weather_current.feels_like;
  const cloudiness = weather_current.clouds;
  const hPa = weather_current.pressure;
  const humidity = weather_current.humidity;
  const visibility_m = weather_current.visibility;
  const wind_speed_mps = weather_current.wind_speed;
  const wind_deg = weather_current.wind_deg;
  const uvi = weather_current.uvi;
  const visibility = weather_current.visibility;

  return (
    <Paper elevation={3} style={{ minWidth: 800, marginTop: "5%"}}>
      <Table aria-label="spanning table" >
        <TableHead>
          <TableRow>
            <TableCell align="left" colSpan={3}>
              <Grid container spacing={0}  alignItems="flex-start" justify="flex-end" direction="row">
                <Grid item xs={false} style={{margin: "auto", display: "block"}}>
                  <LocationIcon style={{ fontSize: 25, height: "auto", margin: "auto", display: "block"}}/>
                </Grid>
                <Grid item xs>
                  <Typography style={{ fontSize: 30 }}>{location}</Typography>
                </Grid>
              </Grid>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          <TableRow>
            <TableCell align="left" colSpan={3}>
              <Grid container spacing={0}  alignItems="flex-start" justify="flex-end" direction="row">
                <Grid item xs={false} style={{margin: "auto", display: "block"}}>
                  <AccessTimeIcon style={{ fontSize: 19, height: "auto", margin: "auto", display: "block"}}/>
                </Grid>
                <Grid item xs>
                  <Typography style={{ fontSize: 18 }}>{date_current_str}</Typography>
                </Grid>
              </Grid>
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell align="left" rowSpan={5} style={{width: "400px"}}>
              <Weather_icon id={weather_id} d={weather_d} style={{width: "300px", margin: "auto", display: "block"}}/>
            </TableCell>
            <TableCell align="center" width="30">
              <img src={Temperature} alt="Temperature" style={{width: "50px", margin: "auto", display: "block"}}/>
            </TableCell>
            <TableCell align="left">
              <Typography  style={{ fontSize: 25 }}>{temp_current.toFixed(1)}°C</Typography>
              <Typography  style={{ fontSize: 15 }}>&emsp;&emsp;Feels like {temp_feel_current.toFixed(1)}°C</Typography>
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell align="center" width="30">
                <img src={Humidity} alt="Humidity" style={{width: "50px", margin: "auto", display: "block"}}/>
            </TableCell>
            <TableCell align="left">
              <Typography  style={{ fontSize: 20 }}>Humidity: {humidity}%</Typography>
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell align="center" width="30">
                <img src={Uvi} alt="Uvi" style={{width: "50px", margin: "auto", display: "block"}}/>
            </TableCell>
            <TableCell align="left">
              <Typography  style={{ fontSize: 20 }}>UV Index: {uvi}</Typography>
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell align="center" width="30">
                <img src={Arrow} alt="Arrow" style={{width: "50px", margin: "auto", display: "block", transform:`rotate(${180+wind_deg}deg)`}}/>
            </TableCell>
            <TableCell align="left">
              <Typography  style={{ fontSize: 20 }}>wind speed: {wind_speed_mps}m/s&emsp;{degToCard(wind_deg)}</Typography>
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell align="center" width="30">
                <img src={Pressure} alt="Pressure" style={{width: "50px", margin: "auto", display: "block"}}/>
            </TableCell>
            <TableCell align="left">
              <Typography  style={{ fontSize: 20 }}>Air pressure: {hPa}hPa</Typography>
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell align="center" width="30">
                <Typography  style={{ fontSize: 20 }}>{capitalizeFirstLetter(weather_description)} on the sky!</Typography>
            </TableCell>
            <TableCell align="center" width="30">
                <img src={Visibility} alt="Visibility" style={{width: "50px", margin: "auto", display: "block"}}/>
            </TableCell>
            <TableCell align="left">
              <Typography  style={{ fontSize: 20 }}>Visibility: {visibility>1000? (visibility/1000).toString() + " Kilometres": visibility.toString()+" Meters"}</Typography>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Paper>
  );
}
