import axios from 'axios'

require('dotenv').config();

async function weatherAPI(lat, lon){
  const weather_req = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${process.env.WEATHER_KEY}&units=metric&exclude=minutely,hourly,daily`;
  const response = await axios.get(weather_req);
  return response.data;
};

module.exports = weatherAPI;
