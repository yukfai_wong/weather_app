module.exports = {
  tts: tts
};
// To install dependencies, run: npm install
import xmlbuilder from 'xmlbuilder'
// request-promise has a dependency on request
import axios from 'axios'
import fs from 'fs'
require('dotenv').config();

// Gets an access token.
function getAccessToken(subscriptionKey) {
    let options = {
        method: 'POST',
        url: 'https://southeastasia.api.cognitive.microsoft.com/sts/v1.0/issueToken',
        headers: {
            'Ocp-Apim-Subscription-Key': subscriptionKey
        }
    }
    return axios(options);
}

// Converts text to speech using the input from readline.
function textToSpeech(accessToken, text) {
    // Create the SSML request.
    let xml_req = xmlbuilder.create('speak')
        .att('version', '1.0')
        .att('xml:lang', 'en-AU')
        .ele('voice')
        .att('name', 'en-AU-NatashaNeural')
        .ele('prosody')
        .att('rate', '1.05')
	      .att('volume', '-30.00%')
        .txt(text)
        .end();
    // Convert the XML into a string to send in the TTS request.
    let data = xml_req.toString();

    let options = {
        method: 'POST',
        baseURL: 'https://southeastasia.tts.speech.microsoft.com/',
        url: 'cognitiveservices/v1',
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'User-Agent': 'TTS-BD',
            'X-Microsoft-OutputFormat': 'audio-24khz-48kbitrate-mono-mp3',
            'Content-Type': 'application/ssml+xml'
        },
        data: data,
        responseType: 'stream'
    }
    return options
};

// Use async and await to get the token before attempting
// to convert text to speech.
async function tts(text) {
    // Reads subscription key from env variable.
    const subscriptionKey = process.env.SPEECH_SERVICE_KEY;
    if (!subscriptionKey) {
        throw new Error('Environment variable for your subscription key is not set.')
    };
    try {
      const accessToken = await getAccessToken(subscriptionKey);
      return textToSpeech(accessToken.data, text);
    } catch (err) {
        console.log(`Something went wrong: ${err}`);
    }
}
