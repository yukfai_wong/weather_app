import axios from 'axios'
require('dotenv').config();

async function ip_location(ip){
  const location_req = `https://api.ipgeolocation.io/ipgeo?apiKey=${process.env.IP_GEOLOCATION_KEY}&ip=${ip}`;
  const response = await axios.get(location_req);
  return response.data;
};

module.exports = ip_location;
