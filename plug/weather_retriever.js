import ip_location from './IPGeolocationAPI';
import weather from './weatherAPI';


function weather_retriever(ip) {
  return new Promise( (resolve, reject) => {
    ip_location(ip)//IP Geolocation API
    .then(function (location) {
      weather(location.latitude, location.longitude)//Openweather One Call API
      .then(function (weather_detail) {
        resolve(weather_detail);//Both sucessful, response with weather information
      })
      .catch(err =>{
        reject(err)//Openweather One Call API Failed
      });
    })
    .catch(err =>{
      reject(err)//IP Geolocation API Failed
    });
  });
}
module.exports = weather_retriever;
