## Weather App ##
### Description ###
Weather App is a webpage that provide weather information in text, image and audio format. This application uses IP Geolocation API to extract user location from its connection IP address. The locaation information is then forwarded to OpenWeather One Call API to grab the regional weather information. Next, the server will build the webpage from the weather information.

Additional, If user request an audio report, the server uses similar approach to fetch weather information and parse into a speech draft. This draft will be decorated using speech synthesis markup and sent to Azure Text-to-Speech REST API. This API uses the neural network to convert the draft into lifelike speech and response with an audio file. When the server receives the audio file, it will redirect the audio file to the client like a middle agent. 

report.pdf documented the application architecture and Server deployement 

### How do I get set up? ###
* cd 
* npm install
* npm run start

