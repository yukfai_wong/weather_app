import express from 'express'
import axios from 'axios'
import dateformat from 'dateformat'
const azure_tts = require('../plug/tts.js');

let router = express.Router();

import weather_retriever from '../plug/weather_retriever.js';


const arr=["North","North-northeast","Northeast","East-northeast","East","East-southeast", "Southeast", "South-southeast","South","South-southwest","Southwest","West-southwest","West","	West-northwest","Northwest","North-northwest"]
//pase degree to cardinal direction
function degToCard(degrees){
  degrees *= 10;
  return arr[Math.round(Math.round((degrees % 3600)/ 225))];
}

function Report_text_constructor(weather_data) {
  //pase the JSON weather daata
  const location = weather_data.timezone.split('/');
  const weather_current = weather_data.current;

  const date_current = new Date(weather_current.dt*1000);
  const date_current_str = dateformat(date_current, "h:MM tt Z, dddd, mmmm d, yyyy")

  const weather = weather_current.weather[0].main;
  const weather_description  = weather_current.weather[0].description ;
  const weather_id = weather_current.weather[0].id;
  const weather_d = weather_current.weather[0].icon.slice(-1);;

  const temp_current = weather_current.temp.toFixed(1);
  const temp_feel_current = weather_current.feels_like.toFixed(0);

  const cloudiness = weather_current.clouds;
  const hPa = weather_current.pressure;
  const humidity = weather_current.humidity;
  const visibility_m = weather_current.visibility;
  const wind_speed_mps = weather_current.wind_speed;
  const wind_deg = weather_current.wind_deg;
  const uvi = weather_current.uvi;
  const visibility = weather_current.visibility;

  //construct speech draft
  const basic_report = `${location[1].replace("_", " ")} is now having ${weather_description} on the sky. `;
  const temp_report = `the temperature is around ${temp_current}°C and feel like ${temp_feel_current}°C. `;
  const humidity_report = `the humidity is around ${humidity}%. `;
  const uvi_report = `the UV index is ${uvi}${uvi>5?", you might need some protection before go out. ":". "}`;
  const wind_report = `the wind speed is currently ${wind_speed_mps} Metres per second and directing to ${degToCard(wind_deg)}. `;
  const pressure_report = `the air pressure is ${hPa} Hectopascals. `
  const visibility_report = `the visibility is ${visibility>1000? (visibility/1000).toFixed(0).toString() + "Kilometres": visibility.toString()+"Meters"}.`;


  return basic_report+temp_report+humidity_report+uvi_report+wind_report+pressure_report+visibility_report;
}

router.get('/', function(req, res, next) {
  weather_retriever(req.ip).then(function(weather_info){//get weather info
    const report_draft = Report_text_constructor(weather_info);//construct audio draft
    azure_tts.tts(report_draft).then(function(option){//build the post request for Azure TTS
      axios(option)//POST the request
      .then(function (response) {
        response.data.pipe(res);//pipe the audio to response the client request
      })
      .catch(err =>{
        //azure post error
        console.log(err.message);
        console.log(err.config);
        res.status(500).send("Error occurr while constructing audio report")
      });
    })
    .catch(err =>{
      //azure POST construct error
      console.log(err.message);
      console.log(err.config);
      res.status(500).send("Error occurr while constructing audio report")
    });
  })
  .catch(err =>{
    //weather fetching error
    console.log(err.message);
    console.log(err.config);
    res.status(500).send("Error occurr while fetching weather data")
  });
});



module.exports = router;
