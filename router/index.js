import express from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ServerStyleSheets, ThemeProvider } from '@material-ui/core/styles';
import Index from '../React/index';
// import theme from '../React/theme';

import weather_retriever from '../plug/weather_retriever.js';

let router = express.Router();




function Elementfiller(html, css) {
  return `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <title>Weather APP</title>
        <style id="jss-server-side">${css}</style>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <script async src="/bundle.js"></script>
      </head>
      <body>
        <div id="root">${html}</div>
      </body>
    </html>
  `;
}

async function PageRander(req, res) {
  let weather_info;
  try{
    weather_info = await weather_retriever(req.ip);
    //retrieve user's local weather info using IP address
  }
  catch(err) {
    console.log(err.message);
    console.log(err.config);
    res.status(500).send("Error occurr while fetching weather data")
    return
  }

  const sheets = new ServerStyleSheets();
  // Render the component to a string.
  // pass weather information to Index component
  const html = ReactDOMServer.renderToString(
    sheets.collect(
      <Index weather_data={weather_info} />
    ),
  );
  // Grab the CSS from our sheets.
  const css = sheets.toString();

  // Send the rendered page back to the client.

  res.send(Elementfiller(html, css));
}
router.get('/', PageRander);

module.exports = router;
