FROM node:12

# Update. Install tools, git, ssh
RUN apt-get update && \
    apt-get install -y apt-utils && \
    apt-get install -y git ssh

# Prepare ssh
RUN mkdir /root/.ssh/ && \
    touch /root/.ssh/id_rsa

# Copy access key for the git repository
# This key will only give read access to the repository, there will be no security issue
RUN echo "-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEA37UhyVD9Gx879tpcrLm1ChZxdOUaZLuVv2hrQLHPXZtvfNYS\noc+6i+hXbAgb+jPYINYYEikW/67vRgnEBHEeJMImAp+iQIwMjWBKrxCvQvIR9eVj\npXpc3umkYmTqwXdEDeinFhdVHg8poIY3KY4Alu/51VUxadc8bnI1z7EvCdlwIyMU\nreRdqqQX16rUsKrwghrAMNdk1iL1XzGL86W6XLmtoSiRBTtmi3hFiu4Hu7DmWyl1\n1EAJPmh2Ccc2uScWh5nxa0uiiGJzVM4bEBmFAeDYHBfiAn2ehenbTxbFb6D3xFIX\nY3KgQXCER4FVPaLoAPLG8oQ0xGB+zVwdEMB2cQIDAQABAoIBAQCz090nAY2U4dTI\nKNxalKXtSAFIIGrJEunbhcrBpK22554FUeiEXoHsuYac9x/WTxqruq562sD8I9mA\nCZWipHPAxLjdXwmErY/hjcg7VkQson7BlfY2LVi+cD6OQeVcQ81XCPBXl8X6deHW\ngk2Rf+8FZj8EuIkSNc0EU5biL+JKHRlVnxW3YaWbqDCN7eC56z+TG/6h9uXg4cmn\nX5fKSaOBeaRnOPLWDap0lW91usilzuU6H3TDkbkSCPp7UyxUWCl9vWj4EkEx1yZ5\nkgIvvji1z6ZAHrLsw26GERrz25aQx9I42LTfeqv6GjQrSeHalOpS4Jc/w9nrbqCT\n6lQjcMNpAoGBAPas8KmMgOHSIVImaicdCFlcv0PRbU5qhgHhtKooFdSyHJ6wKXcX\nMlUHyrlxVZS7p7zDi/SrUhuP/vP/Ye1TJ8AqTJanEjlGvj8cG9YWpwMCKYIDlvk9\nD17GrWrN5rK1BwSbYOWHNutDPD5Drmy8br48dsKgZseWh71YKsUtYaIXAoGBAOgp\n7q6zw8+Eb3hKy86/YQpSBE94+hIbOMIw/GeuL24wby1RXEYqGDAvclJEo230CRHp\nnkSbgqkAIGvBEd91FBPtj+Ajexzx3mXhZpdXbM9541ec6oRmMguR3v1D9Rkcv5/U\nIeMC+94ZamfAP+NXgcC1oT0k6u4A+Q0EmOpFrii3AoGBAMxV1cWdLeXiOtfiGmzs\nD852lbmH8hPTCc7lONL7AZp/MFYDd1W/UWI+Yh/bUhkymXA0S+sRP6K1VWypUGVX\nTMZSQ79IBkCL6Ww9zJE7kn1bfSbF8nnjqqfHzD988G14p6eZTlrT9xpLR8wMJKDO\nNq4/R+FRHdKXTzn5nvRW81lhAoGAOYQJ6aYHYKXCp95/11x6QgBHqRije1DbIuDX\n5ye0AGxnXqbFYWyUSXu3qsHuaxMGHDXwS7brC8tIPOm778wliTpVZoXjviv9mOfa\nbqvt/QiJVe89+kcHJu6ge8YWNn50yrfXHVJj64YIYmOycDQBpShaJbe7q/L5wEvf\nRkq09skCgYEAnCCybPo9vA1Vk+wwwCYaMCsjLJLw7zf/L2X7s6TI2b7oQo7CVGL7\nUEN7fUnlRuaGSdsbprfsrUCP3BlJ7QOepKzV+qb4jvljCOG1HsNfzWad9rH64ZX/\nYg1f86U/Kx8gakoeZAatxqPBJy1Rv8qW/c+WWkoQyirm2UqZGJykY8M=\n-----END RSA PRIVATE KEY-----" >> /root/.ssh/id_rsa

# Create known_hosts, Add bitbuckets key, Clone weather_app into the docker container
RUN chmod 600 /root/.ssh/id_rsa && \
    touch /root/.ssh/known_hosts && \
    ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# Clone weather_app into the docker container
RUN git clone git@bitbucket.org:tomwong1027/weather_app.git

# Change work directory to weather_app
WORKDIR weather_app

# Expose port 3000 for accessing the webserver
EXPOSE 3000

# Install node dependencies
RUN npm install

ENV NODE_ENV=production

# start the command listed in package.json
CMD ["npm", "run", "start"]
