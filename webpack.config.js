const path = require('path');


let react_config = {
  entry: './React/client.js',
  mode: 'production',//could not use production, since optimize will channge parameter name which cause bug
  output: {
    path: path.resolve(__dirname, 'static'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      }
    ],
  },
};

module.exports = [react_config];
